package com.example.splash.ui

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.splash.BR
import com.example.splash.databinding.SplashFragmentBinding
import com.task.data.model.fcm.NotificationModel
import com.task.main.BaseFragment
import com.task.resourses.R
import com.task.utils.AppConstant.NOTIFICATION_KEY
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment<SplashFragmentBinding, SplashViewModel>() {
    val splashViewModel: SplashViewModel by viewModel()
    var notificationModel: NotificationModel? = null

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return com.example.splash.R.layout.splash_fragment
    }

    override fun getViewModel(): SplashViewModel {
        return splashViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rotation: Animation = AnimationUtils.loadAnimation(context, R.anim.rotation)
        viewDataBinding.imageView2.startAnimation(rotation)
        notificationModel =
            getGraphArguments()?.getParcelable(NOTIFICATION_KEY)
        appSettingFinishedObservation()
    }

    private fun appSettingFinishedObservation() {
        mViewModel.isAuthorized.observe(viewLifecycleOwner, {
            if (it) {
                navigateWithAction(com.example.splash.R.id.action_splashFragment_to_navigation_home)
            }
        })
    }
}
