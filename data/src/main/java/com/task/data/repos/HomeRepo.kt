package com.task.data.repos

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.task.data.MagixExceptions
import com.task.data.base.BaseRepo
import com.task.data.model.BaseResponse
import com.task.data.network.ApiFactory
import com.task.data.network.NetworkBoundFileResource
import com.task.data.strategy.DataStrategy
import com.task.network.NetworkFactoryInterface
import com.task.prefrance.SharedPreference
import kotlinx.coroutines.flow.Flow

class HomeRepo(
    apiFactory: ApiFactory,
    sharedPreferences: SharedPreference,
    networkFactory: NetworkFactoryInterface,
) : BaseRepo(apiFactory, sharedPreferences, networkFactory) {


    suspend fun getVenues(latLang: String): Flow<List<BaseResponse.Response.Venue>?> {
        networkFactory.setFileName("result_json.json")
        return object :
            NetworkBoundFileResource<List<BaseResponse.Response.Venue>>(
                networkFactory,
                DataStrategy.Strategies.NETWORK_ONLY,
                fileManager = null,
                call = { apiFactory.getMainApi().getVenues(latLang) }
            ) {
            override fun onFetchFailed(exception: MagixExceptions) {
                checkException(exception)
            }

            override fun String.convert(): List<BaseResponse.Response.Venue> {
                return Gson().fromJson(
                    this,
                    object : TypeToken<List<BaseResponse.Response.Venue>>() {}.type
                )
            }
        }.asFlow()
    }

}