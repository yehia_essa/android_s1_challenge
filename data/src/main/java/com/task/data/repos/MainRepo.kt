package com.task.data.repos

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.task.data.MagixExceptions
import com.task.data.base.BaseRepo
import com.task.data.model.appConfig.AppConfigResult
import com.task.data.network.ApiFactory
import com.task.data.network.NetworkBoundFileResource
import com.task.data.strategy.DataStrategy
import com.task.network.NetworkFactoryInterface
import com.task.prefrance.SharedPreference
import kotlinx.coroutines.flow.Flow

class MainRepo(
    apiFactory: ApiFactory,
    sharedPreferences: SharedPreference,
    networkFactory: NetworkFactoryInterface
) : BaseRepo(apiFactory, sharedPreferences, networkFactory) {

    suspend fun getConfigData(): Flow<AppConfigResult?> {
        networkFactory.setFileName("app_configuration.json")
        return object :
            NetworkBoundFileResource<AppConfigResult>(
                networkFactory,
                DataStrategy.Strategies.NETWORK_ONLY,
                fileManager = null,
                call = { apiFactory.getAppConfiguration().getAppConfigModle() }
            ) {
            override fun onFetchFailed(exception: MagixExceptions) {
                checkException(exception)
            }

            override fun String.convert(): AppConfigResult {
                return Gson().fromJson(
                    this,
                    object : TypeToken<AppConfigResult>() {}.type
                )
            }

        }.asFlow()
    }
}