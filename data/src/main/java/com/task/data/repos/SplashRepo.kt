package com.task.data.repos

import com.task.data.base.BaseRepo
import com.task.data.network.ApiFactory
import com.task.network.NetworkFactoryInterface
import com.task.prefrance.SharedPreference

class SplashRepo(
    apiFactory: ApiFactory,
    sharedPreferences: SharedPreference,
    networkFactory: NetworkFactoryInterface
) : BaseRepo(apiFactory, sharedPreferences, networkFactory) {


}