package com.task.data.constant

class SharedPrefKeys {
    companion object {
        const val USERDATA = "USERDATA"
        const val LOGIN_DATA = "LOGIN_DATA"
        const val FCM_DATA = "FCM_DATA"
        const val SETTINGS_DATA = "SETTINGS_DATA"
        const val TOKEN = "TOKEN"
        const val ONBOARDING_FLAG = "ONBOARDING_FLAG"
    }
}