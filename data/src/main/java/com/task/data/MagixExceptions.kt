package com.task.data

class MagixExceptions(var exception: String?, var statusCode: Int = 0) :
    Exception(exception) {
    enum class ExceptionsTypes(var string: String) {
        NO_INTERNET_CONNECTIVITY("No Internet Connection"),
        UN_AUTHORIZE("UnAuthorize")
    }
}