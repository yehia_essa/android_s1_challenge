package com.task.data.network.interfaces

import com.task.data.model.BaseResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MainApis {


    @GET("search?client_id=HNQW1MZDLHCEM13GOYNE0LEP1YFEMOCDK4NGBQQRJF1232OB&client_secret=BEYASSD0L4PY4UJHFYWHVKA5JJKQJDS2XHH5JY4GEOG4WFUA&v=20200424")
    suspend fun getVenues(
        @Query("ll") latlang: String,
    ): Response<BaseResponse<List<BaseResponse.Response.Venue>>>

}