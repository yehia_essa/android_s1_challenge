package com.task.data.network.interfaces

import com.task.data.model.BaseResponse
import com.task.data.model.appConfig.AppConfigResult
import retrofit2.Response

interface AppConfigurationApis {
    suspend fun getAppConfigModle(): Response<BaseResponse<AppConfigResult>>
}