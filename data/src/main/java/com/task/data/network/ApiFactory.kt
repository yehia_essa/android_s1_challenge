package com.task.data.network

import com.task.data.BuildConfig
import com.task.data.constant.AppConstant
import com.task.data.constant.SharedPrefKeys
import com.task.data.network.interfaces.*
import com.task.network.RetrofitClient
import com.task.prefrance.SharedPreference

class ApiFactory(var appConstant: AppConstant, var sharedPreferences: SharedPreference) {


    fun getMainApi(): MainApis {
        return RetrofitClient
            .Builder()
            .token(sharedPreferences.getString(SharedPrefKeys.TOKEN))
            .build()
            .retrofit(BuildConfig.BASE_URL)
            .create(MainApis::class.java)
    }


    fun getAppConfiguration(): AppConfigurationApis {
        return RetrofitClient
            .Builder()
            .token(sharedPreferences.getString(SharedPrefKeys.TOKEN))
            .build()
            .retrofit("http://dummy.restapiexample.com/api/v1/")
            .create(AppConfigurationApis::class.java)
    }
}