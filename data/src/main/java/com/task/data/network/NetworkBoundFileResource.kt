package com.task.data.network

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.google.gson.Gson
import com.task.data.MagixExceptions
import com.task.data.model.BaseResponse
import com.task.data.strategy.DataStrategy
import com.task.network.NetworkFactory
import com.task.network.NetworkFactoryInterface
import com.task.network.model.Result
import com.task.utils.FileManager
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response

abstract class NetworkBoundFileResource<ResultType : Any>
@MainThread constructor(
    var networkFactory: NetworkFactoryInterface,
    val strategy: DataStrategy.Strategies = DataStrategy.Strategies.DEFAULT_STRATEGY,
    var fileName: String = "",
    var fileManager: FileManager?,
    val call: suspend () -> Response<BaseResponse<ResultType>>
) {
    private suspend fun fetchFromNetwork(dataStrategy: DataStrategy.Strategies): ResultType? {
        if (networkFactory is NetworkFactory) {
            when (val response = networkFactory.makeRequest(call)) {
                is Result.Success -> {
                    if (response.data.meta.code == 200) {
                        if (dataStrategy == DataStrategy.Strategies.NETWORK_ONLY) {
                            return loadDataFromNetwork(response)
                        } else if (dataStrategy == DataStrategy.Strategies.DEFAULT_STRATEGY) {
                            saveCallResultInDB(processResponse(response))
                            return loadDataFromDB()
                        }
                    } else if (response.data.meta.code == 401) {
                        onFetchFailed(MagixExceptions(MagixExceptions.ExceptionsTypes.UN_AUTHORIZE.string, 401))
                        throw MagixExceptions(MagixExceptions.ExceptionsTypes.UN_AUTHORIZE.string, 401)
                    } else {
                        onFetchFailed(MagixExceptions(response.data.meta.errorDetail,response.data.meta.code))
                        throw MagixExceptions(response.data.meta.errorDetail,response.data.meta.code)
                    }
                }
                is Result.Error -> {
                    onFetchFailed(MagixExceptions(response.exception.message))
                    throw MagixExceptions(response.exception.message)
                }
            }
        } else {
            try {
                return dataWrapper(networkFactory.getStringJson().convert())
            } catch (e: Exception) {
                onFetchFailed(MagixExceptions(e.message))
                throw e
            }
        }
        return setEmptyObject()
    }

    private fun setEmptyObject(): ResultType {
        return Any() as ResultType
    }

    private fun loadDataFromNetwork(response: Result.Success<BaseResponse<ResultType>>): ResultType? {
        return dataWrapper(processResponse(response))
    }

    private fun loadDataFromDB(): ResultType? {
        return loadFromDb()
    }

    protected open fun onFetchFailed(exception: MagixExceptions) {}

    suspend fun asFlow(): Flow<ResultType?> {
        return flow {
            if (networkFactory.isNetworkConnected()) {
                emit(fetchFromNetwork(strategy))
            } else {
                if (strategy == DataStrategy.Strategies.NETWORK_ONLY) {
                    onFetchFailed(
                        MagixExceptions(
                            MagixExceptions
                                .ExceptionsTypes.NO_INTERNET_CONNECTIVITY.string
                        )
                    )
                    throw MagixExceptions(
                        MagixExceptions
                            .ExceptionsTypes.NO_INTERNET_CONNECTIVITY.string
                    )
                } else if (strategy == DataStrategy.Strategies.DEFAULT_STRATEGY) {
                    emit(loadDataFromDB())
                }
            }
        }
    }

    @WorkerThread
    protected open fun processResponse(response: Result.Success<BaseResponse<ResultType>>) =
        response.data.response.venues

    @WorkerThread
    private fun saveCallResultInDB(data: ResultType?) {
        val toJson = Gson().toJson(data)
        fileManager?.writeFile(toJson, fileName)
    }

    @MainThread
    protected fun shouldFetch(): Boolean {
        return false
    }

    @MainThread
    protected abstract fun String.convert(): ResultType?

    @MainThread
    private fun loadFromDb(): ResultType? {
        val readFile = fileManager?.readFile(fileName)
        return readFile?.convert()
    }

    @MainThread
    private fun dataWrapper(data: ResultType?): ResultType? {
        return data
    }
}