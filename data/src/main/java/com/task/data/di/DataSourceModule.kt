package com.task.data.di

import com.task.data.constant.AppConstant.Companion.PREF_NAME_KEY
import com.task.network.MockedNetwork
import com.task.network.NetworkFactory
import com.task.prefrance.securedsharedpreferences.SecuredSharedPreference
import com.task.prefrance.sharedpreferences.AppSharedPreference
import com.task.utils.FileManager
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module

val dataSourceModules = module {
    single { NetworkFactory(androidApplication()) }
    single { MockedNetwork(androidApplication()) }
    single { FileManager(androidApplication()) }
    single {
        AppSharedPreference(
            androidApplication(),
            get(named(PREF_NAME_KEY), parameters = null)
        )
    }
    single {
        SecuredSharedPreference(
            androidApplication(),
            get(named(PREF_NAME_KEY))
        )
    }

}