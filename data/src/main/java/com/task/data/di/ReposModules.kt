package com.task.data.di

import com.task.data.constant.AppConstant
import com.task.data.network.ApiFactory
import com.task.data.repos.HomeRepo
import com.task.data.repos.MainRepo
import com.task.data.repos.SplashRepo
import com.task.network.MockedNetwork
import com.task.network.NetworkFactory
import com.task.prefrance.securedsharedpreferences.SecuredSharedPreference
import org.koin.core.qualifier.named
import org.koin.dsl.module

val reposModules = module {

    fun provideApiFactory(
        appConstant: AppConstant,
        sharedPreference: SecuredSharedPreference
    ): ApiFactory {
        return ApiFactory(appConstant, sharedPreference)
    }
    single { provideApiFactory(get(), get()) }

    fun provideMainRepo(
        apiFactory: ApiFactory,
        sharedPreference: SecuredSharedPreference,
        mockedNetwork: MockedNetwork
    ): MainRepo {
        return MainRepo(apiFactory, sharedPreference, mockedNetwork)
    }
    factory { provideMainRepo(get(), get(), get()) }

    fun provideSplashRepo(
        apiFactory: ApiFactory,
        sharedPreference: SecuredSharedPreference,
        networkFactory: NetworkFactory
    ): SplashRepo {
        return SplashRepo(apiFactory, sharedPreference, networkFactory)
    }
    factory { provideSplashRepo(get(), get(), get()) }


    fun provideHomeRepo(
        apiFactory: ApiFactory,
        sharedPreference: SecuredSharedPreference,
        networkFactory: NetworkFactory
    ): HomeRepo {
        return HomeRepo(apiFactory, sharedPreference, networkFactory)
    }
    factory { provideHomeRepo(get(), get(), get()) }

    fun provideMockedHomeRepo(
        apiFactory: ApiFactory,
        sharedPreference: SecuredSharedPreference,
        mockedNetwork: MockedNetwork
    ): HomeRepo {
        return HomeRepo(apiFactory, sharedPreference, mockedNetwork)
    }
    factory(named("homeMocked")) { provideMockedHomeRepo(get(), get(), get()) }

}
