package com.task.data

import androidx.lifecycle.LiveData
import com.task.data.model.appConfig.AppConfigResult

object AppConfig {
    lateinit var appConfigModel: LiveData<AppConfigResult>
}