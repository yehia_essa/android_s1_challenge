package com.task.data.base

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.task.data.MagixExceptions
import com.task.data.constant.SharedPrefKeys.Companion.FCM_DATA
import com.task.data.constant.SharedPrefKeys.Companion.LOGIN_DATA
import com.task.data.constant.SharedPrefKeys.Companion.TOKEN
import com.task.data.model.fcm.FcmModelRequest
import com.task.data.model.login.UserDataResponse
import com.task.data.network.ApiFactory
import com.task.data.network.NetworkBoundFileResource
import com.task.data.strategy.DataStrategy
import com.task.network.NetworkFactoryInterface
import com.task.prefrance.SharedPreference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

open class BaseRepo(
    val apiFactory: ApiFactory,
    val sharedPreference: SharedPreference,
    val networkFactory: NetworkFactoryInterface
) {
    var exceptionMessage = MutableLiveData<Any>()
    var unAuthorized = MutableLiveData<Any>()

    fun checkException(exception: MagixExceptions) {
        if (exception.statusCode == 401) {

        } else {
            exceptionMessage.value = exception.exception + " "
        }
    }


    fun getLoginResponseModelFromSharedPref(): UserDataResponse? {
        return Gson().fromJson(
            sharedPreference.getString(LOGIN_DATA),
            object : TypeToken<UserDataResponse>() {}.type
        )
    }


}