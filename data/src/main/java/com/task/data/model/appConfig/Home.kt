package com.task.data.model.appConfig

import com.google.gson.annotations.SerializedName

data class Home(
    @SerializedName("campusInformationEnabled")
    val campusInformationEnabled: Boolean,
    @SerializedName("campusMapEnabled")
    val campusMapEnabled: Boolean,
    @SerializedName("campusNewsEnabled")
    val campusNewsEnabled: Boolean,
    @SerializedName("chattingEnabled")
    val chattingEnabled: Boolean
)