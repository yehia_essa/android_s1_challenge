package com.task.data.model.appSettings

import com.google.gson.annotations.SerializedName

data class SettingsResponseModel(
    @SerializedName("androidBecaonAccurecy")
    val androidBecaonAccurecy: Any,
    @SerializedName("androidForceUpdate")
    val androidForceUpdate: Any,
    @SerializedName("androidURL")
    val androidURL: Any,
    @SerializedName("androidVersion")
    val androidVersion: Any,
    @SerializedName("applicationID")
    val applicationID: String,
    @SerializedName("applicationTypeID")
    val applicationTypeID: Int,
    @SerializedName("current")
    val current: Any,
    @SerializedName("enableNotification")
    val enableNotification: Any,
    @SerializedName("firebaseServerKey")
    val firebaseServerKey: Any,
    @SerializedName("insertDate")
    val insertDate: String,
    @SerializedName("iosBecaonAccurecy")
    val iosBecaonAccurecy: Any,
    @SerializedName("iosForceUpdate")
    val iosForceUpdate: Any,
    @SerializedName("iosVersion")
    val iosVersion: Any,
    @SerializedName("iosurl")
    val iosurl: Any,
    @SerializedName("leafletURL")
    val leafletURL: String?,
    @SerializedName("videoURL")
    val videoURL: String?,
    @SerializedName("name")
    val name: String,
    @SerializedName("orgID")
    val orgID: String,
    @SerializedName("faceRecognation")
    val faceRecog: Boolean,
    @SerializedName("mood")
    val magixAttendanceMood: String,
    @SerializedName("surveyID")
    val surveyID: Any
)