package com.task.data.model.login

import com.google.gson.annotations.SerializedName

class LoginRequestModel {
    @SerializedName("Email")
    var email: String = ""
    @SerializedName("Password")
    var password: String = ""
}