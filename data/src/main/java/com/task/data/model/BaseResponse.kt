package com.task.data.model


import com.google.gson.annotations.SerializedName

class BaseResponse<T>(
    @SerializedName("meta")
    val meta: Meta,
    @SerializedName("notifications")
    val notifications: List<Notification>,
    @SerializedName("response")
    val response: Response<T>
) {
    class Meta(
        @SerializedName("code")
        val code: Int,
        @SerializedName("requestId")
        val requestId: String,
        @SerializedName("errorType")
        val errorType: String,
        @SerializedName("errorDetail")
        val errorDetail: String
    )

    class Notification(
        @SerializedName("item")
        val item: Item,
        @SerializedName("type")
        val type: String
    ) {
        class Item(
            @SerializedName("unreadCount")
            val unreadCount: Int
        )
    }

    class Response<T>(
        @SerializedName("confident")
        val confident: Boolean,
        @SerializedName("venues")
        val venues: T
    ) {
       data class Venue(
            @SerializedName("allowMenuUrlEdit")
            val allowMenuUrlEdit: Boolean,
            @SerializedName("beenHere")
            val beenHere: BeenHere,
            @SerializedName("canonicalName")
            val canonicalName: String,
            @SerializedName("canonicalPath")
            val canonicalPath: String,
            @SerializedName("canonicalUrl")
            val canonicalUrl: String,
            @SerializedName("categories")
            val categories: List<Category>,
            @SerializedName("contact")
            val contact: Contact,
            @SerializedName("hasPerk")
            val hasPerk: Boolean,
            @SerializedName("hereNow")
            val hereNow: HereNow,
            @SerializedName("id")
            val id: String,
            @SerializedName("location")
            val location: Location,
            @SerializedName("menu")
            val menu: Menu,
            @SerializedName("name")
            val name: String,
            @SerializedName("referralId")
            val referralId: String,
            @SerializedName("specials")
            val specials: Specials,
            @SerializedName("stats")
            val stats: Stats,
            @SerializedName("url")
            val url: String,
            @SerializedName("urlSig")
            val urlSig: String,
            @SerializedName("venueChains")
            val venueChains: List<Any>,
            @SerializedName("venueRatingBlacklisted")
            val venueRatingBlacklisted: Boolean,
            @SerializedName("verified")
            val verified: Boolean
        ) {
            class BeenHere(
                @SerializedName("lastCheckinExpiredAt")
                val lastCheckinExpiredAt: Int
            )

            class Category(
                @SerializedName("icon")
                val icon: Icon,
                @SerializedName("id")
                val id: String,
                @SerializedName("name")
                val name: String,
                @SerializedName("pluralName")
                val pluralName: String,
                @SerializedName("primary")
                val primary: Boolean,
                @SerializedName("shortName")
                val shortName: String
            ) {
                class Icon(
                    @SerializedName("mapPrefix")
                    val mapPrefix: String,
                    @SerializedName("prefix")
                    val prefix: String,
                    @SerializedName("suffix")
                    val suffix: String
                )
            }

            class Contact(
                @SerializedName("facebook")
                val facebook: String,
                @SerializedName("facebookName")
                val facebookName: String,
                @SerializedName("facebookUsername")
                val facebookUsername: String,
                @SerializedName("formattedPhone")
                val formattedPhone: String,
                @SerializedName("phone")
                val phone: String
            )

            class HereNow(
                @SerializedName("count")
                val count: Int,
                @SerializedName("groups")
                val groups: List<Any>,
                @SerializedName("summary")
                val summary: String
            )

            class Location(
                @SerializedName("address")
                val address: String,
                @SerializedName("cc")
                val cc: String,
                @SerializedName("city")
                val city: String,
                @SerializedName("contextGeoId")
                val contextGeoId: Int,
                @SerializedName("contextLine")
                val contextLine: String,
                @SerializedName("country")
                val country: String,
                @SerializedName("crossStreet")
                val crossStreet: String,
                @SerializedName("distance")
                val distance: Int,
                @SerializedName("formattedAddress")
                val formattedAddress: List<String>,
                @SerializedName("labeledLatLngs")
                val labeledLatLngs: List<LabeledLatLng>,
                @SerializedName("lat")
                val lat: Double,
                @SerializedName("lng")
                val lng: Double,
                @SerializedName("state")
                val state: String
            ) {
                class LabeledLatLng(
                    @SerializedName("label")
                    val label: String,
                    @SerializedName("lat")
                    val lat: Double,
                    @SerializedName("lng")
                    val lng: Double
                )
            }

            class Menu(
                @SerializedName("anchor")
                val anchor: String,
                @SerializedName("canonicalPath")
                val canonicalPath: String,
                @SerializedName("externalUrl")
                val externalUrl: String,
                @SerializedName("label")
                val label: String,
                @SerializedName("mobileUrl")
                val mobileUrl: String,
                @SerializedName("type")
                val type: String,
                @SerializedName("url")
                val url: String
            )

            class Specials(
                @SerializedName("count")
                val count: Int,
                @SerializedName("items")
                val items: List<Any>
            )

            class Stats(
                @SerializedName("checkinsCount")
                val checkinsCount: Int,
                @SerializedName("tipCount")
                val tipCount: Int,
                @SerializedName("usersCount")
                val usersCount: Int
            )
        }
    }
}