package com.task.data.model.appConfig

data class OnBoarding(
    val signUpActionEnabled: Boolean,
    val skipActionEnabled: Boolean
)