package com.task.data.model.appConfig

data class Splash(
    val authorizedDirection: String,
    val notAuthorizedDirection: String
)