package com.task.data.model.signup

import com.google.gson.annotations.SerializedName

class SignUpRequestModel {
    @SerializedName("FirstName")
    var firstName: String? = null
    @SerializedName("LastName")
    var lastName: String? = null
    @SerializedName("Email")
    var email: String? = null
    @SerializedName("Password")
    var password: String? = null
    @SerializedName("PhoneNumber")
    var phoneNumber: String? = null
    @SerializedName("dateOfBirth")
    var dateOfBirth: String? = null
    @SerializedName("Nationality")
    var nationality: String? = null
}