package com.task.data.model.appConfig

data class AppConfigResult(
    val login: Login,
    val onBoarding: OnBoarding,
    val splash: Splash,
    val signup: Signup,
    val directoryDetails: DirectoryDetails,
    val home: Home,
    val homeGuest: HomeGuest
)