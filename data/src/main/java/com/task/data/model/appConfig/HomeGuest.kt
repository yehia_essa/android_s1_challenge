package com.task.data.model.appConfig

import com.google.gson.annotations.SerializedName

data class HomeGuest(
    @SerializedName("chattingEnabled")
    val chattingEnabled: Boolean
)