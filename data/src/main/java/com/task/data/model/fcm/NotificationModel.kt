package com.task.data.model.fcm

import android.os.Parcel
import android.os.Parcelable

class NotificationModel(
    var notificationId: String?,
    var title: String?,
    var body: String?,
    var type: String?,
    var customId: String?,
    var time: Long
) : Parcelable {
    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<NotificationModel> =
            object : Parcelable.Creator<NotificationModel> {
                override fun createFromParcel(source: Parcel): NotificationModel =
                    NotificationModel(source)

                override fun newArray(size: Int): Array<NotificationModel?> = arrayOfNulls(size)
            }

        fun createNotificationrModel(it: NotificationModelResponse.ResultBean.ItemsBean): NotificationModel {
            return NotificationModel(
                it.notificationID,
                it.name,
                it.body,
                it.type,
                it.customID,
                0
            )
        }

        fun createNotificationModelFromPush(pushNotificationModelResponse: PushNotificationModelResponse): NotificationModel {
            return NotificationModel(
                pushNotificationModelResponse.notificationID,
                pushNotificationModelResponse.title,
                pushNotificationModelResponse.body,
                pushNotificationModelResponse.type,
                pushNotificationModelResponse.customID,
                pushNotificationModelResponse.dateTime
            )
        }
    }

    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readLong()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(notificationId)
        writeString(title)
        writeString(body)
        writeString(type)
        writeString(customId)
        writeLong(time)
    }
}