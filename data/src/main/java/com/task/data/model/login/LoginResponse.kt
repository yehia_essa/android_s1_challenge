package com.task.data.model.login

data class LoginResponse(
    var token: String? = null
)