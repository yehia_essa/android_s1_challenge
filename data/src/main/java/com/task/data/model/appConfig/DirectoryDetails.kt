package com.task.data.model.appConfig

data class DirectoryDetails(
    val chattingEnabled: Boolean,
    val videoCallEnabled: Boolean
)