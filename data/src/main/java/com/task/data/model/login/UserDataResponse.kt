package com.task.data.model.login

data class UserDataResponse(
    var bio: String? = null,
    var email: String? = null,
    var jobName: String? = null,
    var birthDate: String? = null,
    var customID: String? = null,
    var displayName: String? = "",
    var firstName: String? = null,
    var gender: String? = null,
    var imagePath: String? = null,
    var insertDate: String? = null,
    var isManual: String? = null,
    var jobID: String? = null,
    var lastName: String? = "",
    var qrImagePath: String? = null,
    var registrationCode: String? = null,
    var registrationSource: String? = null,
    var registrationType: String? = null,
    var roomNumber: String? = null,
    var title: String? = null,
    var userType: String? = null,
    var userID: String? = null,
    var faculty: String? = "",
    var year: String? = "",
    var videoURL: String? = null
)