package com.task.data.model.appConfig

enum class AppConfigDirection(var direction: String) {
    HOME("home"),
    ONBOARDING("onBoarding")
}