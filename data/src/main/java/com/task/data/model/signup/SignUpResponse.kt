package com.task.data.model.signup

import com.google.gson.annotations.SerializedName

class SignUpResponse {
    @SerializedName("userID")
    var userID: String? = null
    @SerializedName("jobID")
    var jobID: Any? = null
    @SerializedName("firstName")
    var firstName: String? = null
    @SerializedName("lastName")
    var lastName: String? = null
    @SerializedName("displayName")
    var displayName: Any? = null
    @SerializedName("roomNumber")
    var roomNumber: Any? = null
    @SerializedName("gender")
    var gender: Any? = null
    @SerializedName("title")
    var title: Any? = null
    @SerializedName("type")
    var type: String? = null
    @SerializedName("birthDate")
    var birthDate: Any? = null
    @SerializedName("imagePath")
    var imagePath: String? = null
    @SerializedName("qrImagePath")
    var qrImagePath: String? = null
    @SerializedName("registrationSource")
    var registrationSource: Any? = null
    @SerializedName("registrationType")
    var registrationType: Int = 0
    @SerializedName("bio")
    var bio: Any? = null
    @SerializedName("insertDate")
    var insertDate: String? = null
    @SerializedName("userAccount")
    var userAccount: UserAccountBean? = null
    @SerializedName("userContact")
    var userContact: UserContactBean? = null
    @SerializedName("userSocial")
    var userSocial: Any? = null
    @SerializedName("job")
    var job: Any? = null
    @SerializedName("userInterest")
    var userInterest: List<*>? = null
    @SerializedName("userDevice")
    var userDevice: List<*>? = null

    class UserAccountBean {
        @SerializedName("userID")
        var userID: String? = null
        @SerializedName("profileName")
        var profileName: Any? = null
        @SerializedName("appID")
        var appID: String? = null
        @SerializedName("attend")
        var attend: Any? = null
        @SerializedName("isEligible")
        var isEligible: Any? = null
        @SerializedName("verified")
        var verified: Any? = null
        @SerializedName("type")
        var type: Any? = null
        @SerializedName("insertDate")
        var insertDate: String? = null
        @SerializedName("userLogin")
        var userLogin: UserLoginBean? = null

        class UserLoginBean {
            @SerializedName("userID")
            var userID: String? = null
            @SerializedName("email")
            var email: String? = null
            @SerializedName("password")
            var password: String? = null
            @SerializedName("type")
            var type: Any? = null
        }
    }

    class UserContactBean {
        @SerializedName("userID")
        var userID: String? = null
        @SerializedName("email")
        var email: String? = null
        @SerializedName("address1")
        var address1: Any? = null
        @SerializedName("address2")
        var address2: Any? = null
        @SerializedName("address3")
        var address3: Any? = null
        @SerializedName("homePhone")
        var homePhone: Any? = null
        @SerializedName("mobilePhone")
        var mobilePhone: String? = null
        @SerializedName("faxNumber")
        var faxNumber: Any? = null
        @SerializedName("webSiteURL")
        var webSiteURL: Any? = null
        @SerializedName("companyName")
        var companyName: String? = null
        @SerializedName("insertDate")
        var insertDate: String? = null
    }
}