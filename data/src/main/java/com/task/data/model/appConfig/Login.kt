package com.task.data.model.appConfig

data class Login(
    val facebookLoginActionEnabled: Boolean,
    val forgetPasswordActionEnabled: Boolean,
    val signUpActionEnabled: Boolean
)