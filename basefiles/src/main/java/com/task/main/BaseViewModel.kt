package com.task.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.task.data.base.BaseRepo
import com.task.data.model.login.UserDataResponse

open class BaseViewModel<D : BaseRepo>(
    val dataResources: D
) : ViewModel() {
    var isLoading = MutableLiveData<Boolean>()
    var message = MutableLiveData<Any>()
    var actionDone = MutableLiveData<Any>()


    override fun onCleared() {
        super.onCleared()
    }

    fun setIsLoading(boolean: Boolean) {
        isLoading.postValue(boolean)
    }

    fun actionDone() {
        actionDone.value = true
    }



}
