package com.task.home.ui

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.task.data.model.BaseResponse
import com.task.data.repos.HomeRepo
import com.task.main.BaseViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map

class HomeViewModel(private val homeRepo: HomeRepo) : BaseViewModel<HomeRepo>(homeRepo) {
    val venuLiveData = MutableLiveData<BaseResponse.Response.Venue>()
    suspend fun fetchPosts(latLng: LatLng): Flow<List<BaseResponse.Response.Venue>?> {
        setIsLoading(true)
        return homeRepo.getVenues("${latLng.latitude},${latLng.longitude}").catch { }.map { list -> list?.sortedBy {it.location.distance }}
    }
}
