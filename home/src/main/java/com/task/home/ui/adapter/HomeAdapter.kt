package com.task.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.task.data.model.BaseResponse
import com.task.home.databinding.HomeListItemBinding
import com.task.main.BaseAdapter

class HomeAdapter(private val callBack: CallBack) :
    BaseAdapter<BaseResponse.Response.Venue>(VenuesComparator) {

    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding =
        HomeListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

    override fun bind(binding: ViewDataBinding, position: Int) {
        binding as HomeListItemBinding
        getItem(position)?.let { binding.model = it }
        binding.root.setOnClickListener {
            callBack.onItemClicked(getItem(position))
        }
    }

    interface CallBack {
        fun onItemClicked(item: BaseResponse.Response.Venue)
    }
}