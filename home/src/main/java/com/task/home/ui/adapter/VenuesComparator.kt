package com.task.home.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.task.data.model.BaseResponse

object VenuesComparator : DiffUtil.ItemCallback<BaseResponse.Response.Venue>() {
    override fun areItemsTheSame(
      oldItem: BaseResponse.Response.Venue,
      newItem: BaseResponse.Response.Venue
    ): Boolean {
        // Id is unique.
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
      oldItem: BaseResponse.Response.Venue,
      newItem: BaseResponse.Response.Venue
    ): Boolean {
        return oldItem == newItem
    }
}