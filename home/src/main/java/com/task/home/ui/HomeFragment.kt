package com.task.home.ui

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.task.data.model.BaseResponse
import com.task.home.BR
import com.task.home.R
import com.task.home.databinding.HomeFragmentBinding
import com.task.home.ui.adapter.HomeAdapter
import com.task.main.BaseFragment
import com.task.utils.PermissionUtils
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : BaseFragment<HomeFragmentBinding, HomeViewModel>(), OnMapReadyCallback {

    val homeViewModel: HomeViewModel by viewModel()
    private val LOCATION_PERMISSION_REQUEST_CODE: Int = 5
    private var googleMap: GoogleMap? = null
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    var firstTime = true
    var marker: Marker? = null

    lateinit var homeAdapter: HomeAdapter
    override fun bindingVariable(): Int = BR.viewModel

    override fun layoutId(): Int = R.layout.home_fragment

    override fun getViewModel(): HomeViewModel = homeViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding.map.onCreate(savedInstanceState)
        setMap()
        setupViews()
    }

    private fun fetchPosts(latLng: LatLng) {
        lifecycleScope.launch {
            mViewModel.fetchPosts(latLng).collectLatest { pagingData ->
                hideLoading()
                homeAdapter.submitList(pagingData)
            }
        }
    }

    var onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            viewDataBinding.venueCard.isVisible = false
            marker!!.remove()
            getLastLocation()
            this.isEnabled = true
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationProviderClient.lastLocation.addOnSuccessListener {
            if (googleMap != null) {
                val latLng = LatLng(
                    it.latitude,
                    it.longitude
                )
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                    latLng, 16f
                )
                googleMap!!.animateCamera(cameraUpdate)
            }
        }
        onBackPressedCallback.remove()
    }

    private fun setupViews() {
        homeAdapter = HomeAdapter(object : HomeAdapter.CallBack {
            override fun onItemClicked(item: BaseResponse.Response.Venue) {
                mViewModel.venuLiveData.value = item
                viewDataBinding.venueCard.isVisible = true
                requireActivity().onBackPressedDispatcher.addCallback(onBackPressedCallback)
                if (googleMap != null) {
                    val latLng = LatLng(
                        item.location.lat,
                        item.location.lng
                    )
                    val cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                        latLng, 16f
                    )
                    if (marker != null)
                        marker!!.remove()
                    marker =
                        googleMap!!.addMarker(MarkerOptions().position(latLng).title(item.name))
                    googleMap!!.animateCamera(cameraUpdate)
                    firstTime = false
                }
            }
        })
        viewDataBinding.adapter = homeAdapter
    }

    override fun onMapReady(mMap: GoogleMap?) {
        googleMap = mMap
        requestLocationUpdates()
        googleMap!!.setOnCameraIdleListener {
            val mCenterLatLng = googleMap!!.cameraPosition.target
            val latLng = LatLng(
                mCenterLatLng.latitude,
                mCenterLatLng.longitude
            )
            if (!firstTime)
                fetchPosts(latLng)

        }
    }


    override fun onStart() {
        viewDataBinding.map.onStart()
        super.onStart()

    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates() {
        when {
            PermissionUtils.isAccessFineLocationGranted(requireContext()) -> {
                when {
                    PermissionUtils.isLocationEnabled(requireContext()) -> {
                        googleMap!!.isMyLocationEnabled = true
                        setUpLocationListener()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(requireContext())
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }


    override fun onPause() {
        viewDataBinding.map.onPause()
        super.onPause()
    }

    override fun onStop() {
        viewDataBinding.map.onStop()
        super.onStop()
    }

    override fun onResume() {
        viewDataBinding.map.onResume()
        super.onResume()
    }

    override fun onLowMemory() {
        viewDataBinding.map.onLowMemory()
        super.onLowMemory()
    }

    private fun setMap() {
        viewDataBinding.map.getMapAsync(this)
        fusedLocationProviderClient =
            initFusedLocation()
    }


    @SuppressLint("MissingPermission")
    private fun setUpLocationListener() {
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest =
            LocationRequest.create().setInterval(10000).setFastestInterval(10000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    if (firstTime && googleMap != null && locationResult != null) {
                        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                locationResult.lastLocation.latitude,
                                locationResult.lastLocation.longitude
                            ), 16f
                        )
                        googleMap!!.animateCamera(cameraUpdate)
                        firstTime = false
                    }
                }
            },
            Looper.myLooper()
        )
    }

    private fun initFusedLocation(): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(requireContext()) -> {
                            requestLocationUpdates()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireContext())
                        }
                    }
                } else {
                    showMessage(R.string.location_permission_not_granted)
                }
            }
        }
    }
}