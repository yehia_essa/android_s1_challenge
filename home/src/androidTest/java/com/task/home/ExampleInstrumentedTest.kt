package com.task.home

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.google.android.gms.maps.model.LatLng
import com.task.data.constant.AppConstant
import com.task.data.network.ApiFactory
import com.task.data.repos.HomeRepo
import com.task.home.ui.HomeViewModel
import com.task.network.MockedNetwork
import com.task.prefrance.sharedpreferences.AppSharedPreference
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.KoinTest

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest : KoinTest {
    lateinit var instrumentationContext: Context
    lateinit var homeViewModel: HomeViewModel

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
        val sharedPreferences = AppSharedPreference(instrumentationContext, "")
        val homeRepo = HomeRepo(
            ApiFactory(AppConstant("", ""), sharedPreferences),
            sharedPreferences,
            MockedNetwork(instrumentationContext)
        )
        homeViewModel = HomeViewModel(homeRepo)
    }


    @Test
    fun testIsEmptyList() = runBlocking {
        homeViewModel.fetchPosts(
            LatLng(30.013054932627824, 31.208851709961895)
        ).collect {
            assertEquals(30, it!!.size)
            Thread.sleep(4000)
        }
    }

    @Test
    fun listHasItem() = runBlocking {
        homeViewModel.fetchPosts(
            LatLng(30.013054932627824, 31.208851709961895)
        ).collect {
            assertEquals("جنة الأسماك", it!![0].name)
            Thread.sleep(4000)
        }
    }
}