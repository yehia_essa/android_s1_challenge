package com.task.utils

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat

class Dialog {
    lateinit var dialogg: AlertDialog
    lateinit var dialog: Dialog
    fun showDialogFragment(
        context: Context,
        @LayoutRes layout: Int,
        @ColorRes colorRes: Int
    ): View {
        val builder = AlertDialog.Builder(context)
        val view11 = LayoutInflater.from(context).inflate(layout, null)
        builder.setView(view11)
        dialogg = builder.create()
        dialogg.setCancelable(false)
        val width = context.resources.getDimensionPixelSize(R.dimen._350sdp)
        val height = context.resources.getDimensionPixelSize(R.dimen._350sdp)
        dialogg.window!!.setLayout(width, height)
        dialogg.window!!.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    colorRes
                )
            )
        )
        //        dialogg.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialogg.show()
        return view11
    }

    fun showDialogFragment(context: Context, @LayoutRes layout: Int): View {
        dialog = Dialog(context, R.style.NewDialog)
        dialog.setContentView(layout)
        dialog.window!!.setLayout(MATCH_PARENT, MATCH_PARENT)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        return dialog.window!!.decorView
    }

    fun dismiss() {
        dialog.dismiss()
    }
}