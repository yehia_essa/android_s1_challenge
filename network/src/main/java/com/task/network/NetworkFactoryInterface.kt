package com.task.network

import com.task.network.model.Result
import retrofit2.Response

interface NetworkFactoryInterface {
    fun isNetworkConnected(): Boolean
    suspend fun <T : Any> makeRequest(
        call: suspend () -> Response<T>
    ): Result<T>?

    fun setFileName(assetFileName: String) {}

    fun getStringJson(): String {
        return ""
    }
}