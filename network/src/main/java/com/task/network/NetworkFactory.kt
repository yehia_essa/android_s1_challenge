package com.task.network

import android.content.Context
import android.net.ConnectivityManager
import retrofit2.Response
import java.io.IOException
import com.task.network.model.Result

class NetworkFactory(var context: Context) : NetworkFactoryInterface {

    override fun isNetworkConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }

    override suspend fun <T : Any> makeRequest(
        call: suspend () -> Response<T>
    ): Result<T>? {
        return safeApiResult(call)
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>
    ): Result<T> = try {
        val result = call()
        when (result.isSuccessful) {
            false -> Result.Error(IOException("Error Occurred during getting safe Api result"))
            else -> Result.Success(result.body()!!)
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
        Result.Error(ex)
    }
}