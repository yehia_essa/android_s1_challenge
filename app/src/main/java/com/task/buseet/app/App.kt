package com.task.buseet.app

import androidx.multidex.MultiDexApplication
import com.task.buseet.di.allFeatures
import com.task.buseet.di.appModule
import com.task.data.di.dataSourceModules
import com.task.data.di.reposModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : MultiDexApplication() {

    companion object {
        lateinit var application: App
    }

    override fun onCreate() {
        super.onCreate()
        application = this
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            val modules = ArrayList(allFeatures)
            modules.addAll(listOf(appModule, reposModules, dataSourceModules))
            modules(
                modules
            )
        }
    }
}
