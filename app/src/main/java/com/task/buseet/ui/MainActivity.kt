package com.task.buseet.ui

import android.os.Bundle
import androidx.navigation.NavArgument
import com.task.app.BR
import com.task.app.R
import com.task.app.databinding.ActivityMainBinding
import com.task.main.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val build = NavArgument.Builder().setDefaultValue(intent?.extras ?: Bundle()).build()
        navigation.graph.addArgument("bundle", build)
    }

    override fun bindingVariable(): Int {
        return BR.viewModel
    }

    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun controllerId(): Int {
        return R.id.mainController
    }

    override fun getViewModel(): MainViewModel {
        return mainViewModel
    }
}