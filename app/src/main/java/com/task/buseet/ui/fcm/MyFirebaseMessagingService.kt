package com.task.buseet.ui.fcm

import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.task.app.R
import com.task.buseet.ui.MainActivity
import com.task.data.model.fcm.NotificationModel
import com.task.data.model.fcm.PushNotificationModelResponse
import org.json.JSONObject

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            val gson = Gson()
            try {
                val json = JSONObject(remoteMessage.data as Map<*, *>)
                Log.e(TAG, "Data Payload: $json")
                val pushNotificationModelResponse = gson.fromJson(
                    json.toString(),
                    PushNotificationModelResponse::class.java
                )
                handleDataMessage(pushNotificationModelResponse)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }
        }
    }

    private fun handleDataMessage(pushNotificationModelResponse: PushNotificationModelResponse) {
        try {
            NotificationModel.createNotificationModelFromPush(pushNotificationModelResponse)
            // app is in splash, show the notification in notification tray
            // check for image attachment
            NewNotificationMessage.notify(
                this,
                Intent(this, MainActivity::class.java),
                NotificationModel.createNotificationModelFromPush(pushNotificationModelResponse),
                R.mipmap.ic_launcher
            )
        } catch (e: Exception) {
            Log.e("Exception: ", e.message!!)
        }
    }

    companion object {
        private val TAG = MyFirebaseMessagingService::class.java.simpleName
    }
}
