package com.task.buseet.ui

import androidx.lifecycle.liveData
import com.task.data.AppConfig
import com.task.data.repos.MainRepo
import com.task.main.BaseViewModel
import kotlinx.coroutines.flow.collect

class MainViewModel(val mainRepo: MainRepo) :
    BaseViewModel<MainRepo>(mainRepo) {
    init {
        AppConfig.appConfigModel = liveData {
            mainRepo.getConfigData().collect {
                emit(it!!)
            }
        }
    }
}
