package com.task.buseet.di

import com.example.splash.di.splashModule
import com.task.home.di.homeModule

// TODO :- IMPORTANT NOTE: PLEASE MAKE IT IN SEPARATE LINE AS IT IS NOW TO SOLVE ITS CONFLICTING EASILY
var allFeatures = listOf(
    splashModule,
    homeModule
)
