package com.task.buseet.di

import com.task.app.BuildConfig
import com.task.buseet.ui.MainViewModel
import com.task.data.constant.AppConstant
import com.task.data.constant.AppConstant.Companion.DATABASE_NAME_KEY
import com.task.data.constant.AppConstant.Companion.PREF_NAME_KEY
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {
    single {
        AppConstant(
            "",
            ""
        )
    }

    factory (named(PREF_NAME_KEY)) { BuildConfig.PrefName }
    single(named(DATABASE_NAME_KEY)) { BuildConfig.DatabaseName }

    viewModel { MainViewModel(get()) }
}
